'user strict';

/**
 * Dependencies
 */
var fs = require('fs');
var path = require('path');
var mkdirp = require('mkdirp');
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

/**
 * Main file
 */
var main = './src/index.js';
var watchFiles = ['./src/*.js', './src/api/*.js', './src/core/*.js', './src/utils/*.js'];
var destinyFilename = 'minicarto.min.js';
var destinyPath = './build/';

/**
 * Create build directory
 */
mkdirp(destinyPath);

/**
 * Browserify task
 */
gulp.task('browserify', function() {
  var file = path.resolve(main);
  browserify(file)
    .bundle()
    .pipe(source(destinyFilename))
    .pipe(buffer()) // comment this
    .pipe(uglify()) // and this if you want debug javascript not minified on browser
    .pipe(gulp.dest(destinyPath));
});

/**
 * Build task
 */
gulp.task('build', ['browserify']);

gulp.task('watch', function() {
  gulp.watch(watchFiles, ['browserify']);
});
