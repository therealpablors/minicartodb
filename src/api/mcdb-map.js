//  minicartodb.js 1.0.0
//  mcdb map
//  https://therealpablors@bitbucket.org/therealpablors/minicartodb.git
//  (c) 2017 Pablo Rodríguez, @therealpablors
//  Carto Test Frontend developer 2017
//  minicarto may be freely distributed under the MIT license.

'use strict';

/**
 * Dependencies
 */
var mcdbJSonParser = require('../utils/mcdb-parser');
var _ = require('underscore');

function McdbMap(mapProvider) {
  this.mapProvider = mapProvider;
  this._layergroupid = 0,
  this._xyzTemplateURL = 'http://ashbu.cartocdn.com/{username}/api/v1/map/{layergroupid}/{index}/{z}/{x}/{y}.png'
  return this;
}

var createURLSxyz = function(urlTemplate, data) {
  _.templateSettings = {
    interpolate: /\{(.+?)\}/g
  };
  data.x = '{x}';
  data.y = '{y}';
  data.z = '{z}';
  var template = _.template(urlTemplate);
  return template(data);
};

McdbMap.prototype.create = function(selector, mapInfo, map) {
  var keys = Object.keys(map || {});
  switch(this.mapProvider) {
  case 'leaflet':
    var L = window.L;
    if (keys.length > 0) {
      map.remove();
    }
    return L.map(selector).setView(mapInfo.center, mapInfo.zoom);
    break;
  case 'openlayers':
    var ol = window.ol;
    if (keys.length > 0) {
      map.setTarget(null);
      map = null;
    }
    return new ol.Map({
      target: selector,
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat(mapInfo.center),
        zoom: mapInfo.zoom
      })
    });
    break;
  default:
    break;
  }
};

McdbMap.prototype.createLayers = function(data, config) {
  var self = this;
  self._layergroupid = data.layergroupid;
  switch(this.mapProvider) {
  case 'leaflet':
    var L = window.L;
    return L.tileLayer(self._xyzTemplateURL, {
      username: config['user_name'],
      layergroupid: self._layergroupid,
      maxZoom: config.maxZoom || 18,
      minZoom: config.minZoom || 1,
      index: _.range(data.metadata.layers.length)
    });
    break;
  case 'openlayers':
    var ol = window.ol;
    var xyzURL = createURLSxyz(self._xyzTemplateURL, {
      username: config['user_name'],
      layergroupid: self._layergroupid,
      maxZoom: config.maxZoom || 18,
      minZoom: config.minZoom || 1,
      index: _.range(data.metadata.layers.length)
    });
    return new ol.layer.Tile({
      source: new ol.source.XYZ({
        url: xyzURL
      })
    });
    break;
  default:
    break;
  }
};

McdbMap.prototype.createGroupLayers = function(data, config) {
  var self = this;
  var layers = _.range(data.metadata.layers.length);
  var layersGroup = {};
  self._layergroupid = data.layergroupid;
  var info = {
    username: config['user_name'],
    layergroupid: self._layergroupid,
    maxZoom: config.maxZoom || 18,
    minZoom: config.minZoom || 1
  };
  switch(this.mapProvider) {
  case 'leaflet':
    var L = window.L;
    _.each(layers, function(layer, index) {
      info.index = index;
      layersGroup['layer ' + index] = L.tileLayer(self._xyzTemplateURL, info);
    });
    var baseLayer = this.createLayers(data, config);
    return {
      control: L.control.layers(layersGroup),
      layersGroup: layersGroup,
      baseLayer: baseLayer
    };
    break;
  case 'openlayers':
    var ol = window.ol;
    _.each(layers, function(layer, index) {
      info.index = index;
      var xyzURL = createURLSxyz(self._xyzTemplateURL, info);
      layersGroup['layer' + index] = new ol.layer.Tile({
        source: new ol.source.XYZ({
          url: xyzURL
        })
      });
    });
    var baseLayer = this.createLayers(data, config);
    return {
      layersGroup: layersGroup,
      baseLayer: baseLayer
    };
    break;
  default:
    break;
  }
};

McdbMap.prototype.setOpacityToLayer = function(layerIndex, opacity, layers) {
  var keys = Object.keys(layers);
  switch(this.mapProvider) {
  case 'leaflet':
    return layers[keys[layerIndex]].setOpacity(opacity);
    break;
  case 'openlayers':
    return layers[keys[layerIndex]].setOpacity(opacity);
    break;
  default:
    break;
  }
};

module.exports = McdbMap;
