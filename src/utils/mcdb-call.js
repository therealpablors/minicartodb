//  minicartodb.js 0.1.0
//  call
//  https://therealpablors@bitbucket.org/therealpablors/minicartodb.git
//  (c) 2017 Pablo Rodríguez, @therealpablors
//  Carto Test Frontend developer 2017
//  minicarto may be freely distributed under the MIT license.

'use strict';

var emitter = require('./mcdb-events');
var emitterMessages = emitter.eventBus._messages;
var extend = require('util')._extend;

var defaults = {
  'crossOrigin': true,
  'type': 'GET',
  'dataType': 'json',
  'contentType': 'application/json',
  'url': '',
  'data': {},
  'ajaxPOSTEventMessage': 'ajaxPOST'
};

var McdbCall = function(options) {
  options = options || {};
  this._options = extend({}, defaults);
  extend(this._options, options);
};

McdbCall.prototype.ajaxPOST = function(url, data) {
  var self = this;
  $.ajax({
    crossOrigin: self._options.crossOrigin,
    type: 'POST',
    dataType: self._options.dataType,
    contentType: self._options.contentType,
    url: url,
    data: JSON.stringify(data),
    success: function (data) {
      emitter.eventBus.sendEvent(emitterMessages.getCartoData, data);
    }
  });
};

module.exports = McdbCall;
