//  minicartodb.js 0.1.0
//  json Parser
//  https://therealpablors@bitbucket.org/therealpablors/minicartodb.git
//  (c) 2017 Pablo Rodríguez, @therealpablors
//  Carto Test Frontend developer 2017
//  minicarto may be freely distributed under the MIT license.

'use strict';

var _ = require('underscore');

var cartoDBLayer = {
  "type": "CartoDB",
  "options": {
    "sql": "select * from european_countries_e",
    "cartocss": "/** choropleth visualization */\n\n#european_countries_e{\n  polygon-fill: #FFFFB2;\n  polygon-opacity: 0.8;\n  line-color: #FFF;\n  line-width: 1;\n  line-opacity: 0.5;\n}\n#european_countries_e [ area <= 1638094] {\n   polygon-fill: #B10026;\n}\n#european_countries_e [ area <= 55010] {\n   polygon-fill: #E31A1C;\n}\n#european_countries_e [ area <= 34895] {\n   polygon-fill: #FC4E2A;\n}\n#european_countries_e [ area <= 12890] {\n   polygon-fill: #FD8D3C;\n}\n#european_countries_e [ area <= 10025] {\n   polygon-fill: #FEB24C;\n}\n#european_countries_e [ area <= 9150] {\n   polygon-fill: #FED976;\n}\n#european_countries_e [ area <= 5592] {\n   polygon-fill: #FFFFB2;\n}",
    "cartocss_version": "2.1.1"
  }
};

var mcdbJSONParser = {};

mcdbJSONParser._getMapInfoErrorMessage = 'vizjon is not setted correctly';
mcdbJSONParser._setSQLErrorMessage = 'vizjon does not have a CartoDB layer';
mcdbJSONParser._cartoDBLayer = cartoDBLayer;

mcdbJSONParser.getMapInfo = function (vizjson) {
  var mapInfo = {};
  try {
    mapInfo.center = JSON.parse(vizjson.center);
    mapInfo.zoom = vizjson.zoom || 1;
    mapInfo.mapProvider = vizjson['map_provider'] || 'default';
  } catch(exception) {
    throw new Error(mcdbJSONParser._getMapInfoErrorMessage);
  }
  return mapInfo;
};

mcdbJSONParser.getLayersData = function (vizjson) {
  var layers = [];
  var parsedLayers = [];

  if (vizjson.hasOwnProperty('layers') && Array.isArray(vizjson.layers)) {
    layers = layers.concat(layers, vizjson.layers);
    _.each(layers, function(layer) {
      var layerParsed = _.extend({}, layer);
      switch(layer.type) {
      case 'tiled':
        layerParsed.type = 'http';
        parsedLayers.push(layerParsed);
        break;
      case 'CartoDB':
        layerParsed.type = 'mapnik';
        parsedLayers.push(layerParsed);
        break;
      default:
        break;
      }
    });
  }
  return {
    layers: parsedLayers
  };
};

mcdbJSONParser.setSQL = function(sql, vizjson) {
  var notCartoDBLayer = true;
  if (vizjson.hasOwnProperty('layers') && Array.isArray(vizjson.layers)) {
    _.each(vizjson.layers, function(layer) {
      if (layer.type === 'CartoDB') {
        layer.options.sql = sql;
        notCartoDBLayer = false;
      }
    });
  } else {
    vizjson.layers = [];
  }
  if (notCartoDBLayer) {
    cartoDBLayer.options.sql = sql;
    vizjson.layers.push(cartoDBLayer);
  }
};

module.exports = mcdbJSONParser;
