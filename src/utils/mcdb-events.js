//  minicartodb.js 0.1.0
//  events
//  https://therealpablors@bitbucket.org/therealpablors/minicartodb.git
//  (c) 2017 Pablo Rodríguez, @therealpablors
//  Carto Test Frontend developer 2017
//  minicarto may be freely distributed under the MIT license.

'use strict';

var eventEmitter = require('events').EventEmitter;
var inherits = require('util').inherits;

var messages = {
  getCartoData: 'getCartoData',
  mapProviderLoaded: 'mapProviderLoaded'
};

function Event() {
  this._messages = messages;
  eventEmitter.call(this);
  return this;
}

inherits(Event, eventEmitter);

Event.prototype.sendEvent = function (type, data) {
  this.emit(type, data);
};

var eventBus = new Event();

module.exports = {
  emitter: Event,
  eventBus: eventBus
};
