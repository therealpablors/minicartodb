require('./utils/mcdb-call.spec');
require('./utils/mcdb-events.spec');
require('./utils/mcdb-parser.spec');
require('./core/mcdb.spec');
require('./api/mcdb-map.spec');
