//  minicartodb.js 1.0.0
//  mcdb tests
//  [url bitbucket]
//  (c) 2017 Pablo Rodríguez, @therealpablors
//  Carto Test Frontend developer 2017
//  minicarto may be freely distributed under the MIT license.

'use strict';
var _ = require('underscore');

var ajaxPOSTCalled = {};
var vizjson = {
  zoom: '2',
  center: [1,2],
  'maps_api_config': {
    'user_name': 'fake-user'
  }
};

var proxyquire = require('proxyquireify')(require);
var stubs = {
  '../api/mcdb-map': {
    'create': jasmine.createSpy('createMapSpy').and.callFake(function(selector, mapInfo, mapProvider, nativeMap) {
      return {
        selector: selector,
        mapInfo: mapInfo,
        mapProvider: mapProvider,
        nativeMap: nativeMap
      };
    }),
    'createLayers': jasmine.createSpy('createLayersSpy'),
    'createGroupLayers': jasmine.createSpy('createGroupLayersSpy').and.callFake(function(data, api, provider) {
      var obj = {
        layersGroup: [{
          tile: 'fake'
        }]
      };
      if (provider === 'leaflet') {
        obj.control = 'fake-control';
      }

      return obj;
    }),
    'setOpacityToLayer': jasmine.createSpy('setOpacityToLayerSpy')
  },
  '../utils/mcdb-call': function() {
    return {
      McdbCall: function(options) {
        return options;
      },
      ajaxPOST: jasmine.createSpy('createMapSpy').and.callFake(function(urlAPI, layers) {
        ajaxPOSTCalled.urlAPI = urlAPI;
        ajaxPOSTCalled.layers = layers;
      })
    };
  },
  '../utils/mcdb-parser': {
    'getMapInfo': function(data) {
      return {
        zoom: data.zoom,
        center: data.center,
        mapProvider: data.mapProvider || 'default'
      };
    },
    'getLayersData': function() {
      return 'fake-layers';
    },
    'setSQL': jasmine.createSpy('setSQLSpy')
  },
  '../utils/mcdb-events': {
    'eventBus': {
      '_messages': {
        'mapProviderLoaded': 'mapProviderLoaded'
      },
      'on': function(message, callback) {
        if(message === 'mapProviderLoaded') {
          return callback();
        }
      },
      'sendEvent': function(customMessage) {
        this.on(customMessage);
      }
    }
  }
};
var Mcdb = proxyquire('../../src/core/mcdb', stubs);
var mcdb = new Mcdb();

describe('mcdb module', function() {
  describe('constructor', function() {
    it('should have correct properties', function() {
      expect(mcdb.hasOwnProperty('VERSION')).toBeTruthy();
      expect(mcdb.hasOwnProperty('_vizjson')).toBeTruthy();
      expect(mcdb.hasOwnProperty('_htmlSelector')).toBeTruthy();
      expect(mcdb.hasOwnProperty('_nativeMap')).toBeTruthy();
      expect(mcdb.hasOwnProperty('_dependencies')).toBeTruthy();
      expect(mcdb.hasOwnProperty('_mapProviderError')).toBeTruthy();
    });
  });

  describe('createMap', function() {
    var selector = 'fake-selector';
    var vizjsonWithOpenLayers = {
      zoom: '2',
      center: [1,2],
      mapProvider: 'openlayers'
    };
    var vizjsonWithLeaflet = {
      zoom: '2',
      center: [1,2],
      mapProvider: 'leaflet'
    };

    var mapWithNoProvider;
    var mapWithOpenLayersProvider;
    var mapWithLeafletProvider;

    beforeAll(function() {
      mcdb.createMap(selector, vizjson);
      mapWithNoProvider = mcdb._options.mapProvider;

      mcdb.createMap(selector, vizjsonWithOpenLayers);
      mapWithOpenLayersProvider = mcdb._options.mapProvider;

      mcdb.createMap(selector, vizjsonWithLeaflet);
      mapWithLeafletProvider = mcdb._options.mapProvider;
    });

    it('should set default map provider (leaflet) if getMapInfo returns default map provider', function() {
      expect(mapWithNoProvider).toEqual('leaflet');
    });

    it('should set map provider to openlayers if vizjson has a mapProvider property with openlayers value', function() {
      expect(mapWithOpenLayersProvider).toEqual('openlayers');
    });

    it('should set map provider to leaflet if vizjson has a mapProvider property with leaflet value', function() {
      expect(mapWithLeafletProvider).toEqual('leaflet');
    });

    it('should create a map', function() {
      expect(stubs['../api/mcdb-map'].create).toHaveBeenCalled();
    });
  });

  describe('createViz', function() {
    var createMapOriginal = mcdb.createMap;
    var getCartoDataOriginal = mcdb.getCartoData;

    var selector = 'fake-selector';
    var options = {fake: 'fake'};

    beforeAll(function() {
      mcdb.createMap = jasmine.createSpy('createMapSpy');
      mcdb.getCartoData = jasmine.createSpy('cartoDabaSpy');

      mcdb.createVis(selector, vizjson, options);
    });

    it('should update properties', function() {
      expect(mcdb._options.hasOwnProperty('fake')).toBeTruthy();
      expect(mcdb._htmlSelector).toEqual(selector);
      expect(mcdb._vizjson).toEqual(vizjson);
      expect(mcdb._mapsApiConfig).toEqual(vizjson['maps_api_config']);
    });

    it('should create a map with correct params', function() {
      expect(mcdb.createMap).toHaveBeenCalledWith(selector, vizjson);
    });

    it('should call carto API to get layers information', function() {
      expect(mcdb.getCartoData).toHaveBeenCalled();
    });

    afterAll(function() {
      mcdb.createMap = createMapOriginal;
      mcdb.getCartoData = getCartoDataOriginal;
    });
  });

  describe('getCartoData', function() {
    beforeAll(function() {
      mcdb.getCartoData();
    });

    it('should call ajaxPOST with correct params', function() {
      expect(ajaxPOSTCalled.urlAPI).toEqual('https://fake-user.carto.com/api/v1/map');
      expect(ajaxPOSTCalled.layers).toEqual('fake-layers');
    });
  });

  describe('createLayers', function() {
    var createGroupLayersOriginal = mcdb.createGroupLayers;

    beforeAll(function() {
      mcdb.createGroupLayers = jasmine.createSpy('createGroupLayersSpy');
      mcdb._nativeMap.addLayer = jasmine.createSpy('addLayerSpy');
      mcdb.createLayers({});
    });

    it('should call createLayers when _options does not have groupLayers === false', function() {
      expect(stubs['../api/mcdb-map'].createLayers).toHaveBeenCalled();
      expect(mcdb._nativeMap.addLayer).toHaveBeenCalled();
    });

    it('should call createGroupLayers when _options has groupLayers === true', function() {
      mcdb._options.groupLayers = true;
      mcdb.createLayers({});
      expect(mcdb.createGroupLayers).toHaveBeenCalled();
    });

    afterAll(function() {
      mcdb.createGroupLayers = createGroupLayersOriginal;
    });
  });

  describe('createGroupLayers', function() {
    beforeEach(function() {
      mcdb._nativeMap.addLayer = jasmine.createSpy('addLayerSpy');
      mcdb._nativeMap.addControl = jasmine.createSpy('addControlSpy');
    });

    it('should add layers and control if map Provider is leaflet', function() {
      mcdb.createGroupLayers({});
      expect(mcdb._nativeMap.addLayer).toHaveBeenCalled();
      expect(mcdb._nativeMap.addControl).toHaveBeenCalledWith('fake-control');
    });

    it('should add layers and not control if map Provider is openlayers', function() {
      mcdb._options.mapProvider = 'openlayers';
      mcdb.createGroupLayers({});
      expect(mcdb._nativeMap.addLayer).toHaveBeenCalled();
      expect(mcdb._nativeMap.addControl).not.toHaveBeenCalled();
    });
  });

  describe('enableLayer', function() {
    var index = 1;
    var enabled = 1;

    beforeAll(function() {
      mcdb.enableLayer(1);
    });

    it('should enable layer', function(){
      expect(stubs['../api/mcdb-map'].setOpacityToLayer).toHaveBeenCalledWith(index, enabled, jasmine.any(Object), jasmine.any(String));
    });
  });

  describe('disableLayer', function() {
    var index = 1;
    var disabled = 0;

    beforeAll(function() {
      mcdb.disableLayer(1);
    });

    it('should disable layer', function(){
      expect(stubs['../api/mcdb-map'].setOpacityToLayer).toHaveBeenCalledWith(index, disabled, jasmine.any(Object), jasmine.any(String));
    });
  });

  describe('setSQL', function() {
    var createVisOriginal = mcdb.createVis;

    beforeAll(function() {
      mcdb.createVis = jasmine.createSpy('createVis');
      mcdb.setSQL('fake-query');
    });

    it('should update sql from vizjson and call createVis to create map and fetch tiles', function() {
      expect(stubs['../utils/mcdb-parser'].setSQL).toHaveBeenCalledWith('fake-query', mcdb._vizjson);
      expect(mcdb.createVis).toHaveBeenCalledWith(mcdb._htmlSelector, mcdb._vizjson);
    });

    afterAll(function() {
      mcdb.createVis = createVisOriginal;
    });
  });
});
