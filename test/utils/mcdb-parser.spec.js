//  minicartodb.js 1.0.0
//  jsonParser utils tests
//  [url bitbucket]
//  (c) 2017 Pablo Rodríguez, @therealpablors
//  Carto Test Frontend developer 2017
//  minicarto may be freely distributed under the MIT license.

'use strict';

var _ = require('underscore');
var mcdbJsonParser = require('../../src/utils/mcdb-parser');

describe('mcdb-parser module', function() {
  var vizjson = {
    "center": "[52.5897007687178, 52.734375]",
    "zoom": 4,
    "maps_api_config": {
      "user_name": "documentation",
      "maps_api_template": "http://{user}.cartodb.com:80"
    },
    "layers": [
      {
        "type": "tiled",
        "options": {
          "urlTemplate": "http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png",
          "minZoom": "0",
          "maxZoom": "18",
          "attribution": "&copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors"
        }
      },
      {
        "type": "CartoDB",
        "options": {
          "sql": "select * from european_countries_e",
          "cartocss": "/** choropleth visualization */\n\n#european_countries_e{\n  polygon-fill: #FFFFB2;\n  polygon-opacity: 0.8;\n  line-color: #FFF;\n  line-width: 1;\n  line-opacity: 0.5;\n}\n#european_countries_e [ area <= 1638094] {\n   polygon-fill: #B10026;\n}\n#european_countries_e [ area <= 55010] {\n   polygon-fill: #E31A1C;\n}\n#european_countries_e [ area <= 34895] {\n   polygon-fill: #FC4E2A;\n}\n#european_countries_e [ area <= 12890] {\n   polygon-fill: #FD8D3C;\n}\n#european_countries_e [ area <= 10025] {\n   polygon-fill: #FEB24C;\n}\n#european_countries_e [ area <= 9150] {\n   polygon-fill: #FED976;\n}\n#european_countries_e [ area <= 5592] {\n   polygon-fill: #FFFFB2;\n}",
          "cartocss_version": "2.1.1"
        }
      },
      {
        "options": {
          "urlTemplate": "http://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png",
          "minZoom": "0",
          "maxZoom": "18",
          "attribution": "&copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors"
        },
        "type": "tiled"
      }
    ]
  };

  describe('getMapInfo', function() {
    var getInfoMapData = {};
    var getInfoMapDataWithDefaultValues = {};
    var vizjsonWithoutZoomAndProvider = {
      "center": "[52.5897007687178, 52.734375]"
    };

    beforeAll(function() {
      getInfoMapData = mcdbJsonParser.getMapInfo(vizjson);
      getInfoMapDataWithDefaultValues = mcdbJsonParser.getMapInfo(vizjsonWithoutZoomAndProvider);
    });

    it('should throw error when getMapInfo does not have a json with center and zoom properties', function() {
      expect(mcdbJsonParser.getMapInfo).toThrowError(mcdbJsonParser._getMapInfoErrorMessage);
    });
    it('should returns a json object with center, zoom and mapProvider properties', function() {
      expect(_.has(getInfoMapData, 'center')).toBeTruthy();
      expect(_.has(getInfoMapData, 'zoom')).toBeTruthy();
      expect(_.has(getInfoMapData, 'mapProvider')).toBeTruthy();
    });
    it('should returns default values for undefined properties', function() {
      expect(getInfoMapDataWithDefaultValues).toEqual({
        center: JSON.parse(vizjsonWithoutZoomAndProvider.center),
        zoom: 1,
        mapProvider: 'default'
      });
    });
  });

  describe('getLayersData', function() {
    var getLayersDataWitNoLayersProperty = {};
    var getLayersDataWithNoArrayObjectIntoLayersProperty = {};
    var getLayersDataWithOneWrongProperty = {};
    var vizjsonWithTwoGoodsPropertiesAndOneWrongProperty = {
      "layers": [
        {
          "type": "wrong",
          "options": {
            "urlTemplate": "http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png",
            "minZoom": "0",
            "maxZoom": "18",
            "attribution": "&copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors"
          }
        },
        {
          "type": "CartoDB",
          "options": {
            "sql": "select * from european_countries_e",
            "cartocss": "/** choropleth visualization */\n\n#european_countries_e{\n  polygon-fill: #FFFFB2;\n  polygon-opacity: 0.8;\n  line-color: #FFF;\n  line-width: 1;\n  line-opacity: 0.5;\n}\n#european_countries_e [ area <= 1638094] {\n   polygon-fill: #B10026;\n}\n#european_countries_e [ area <= 55010] {\n   polygon-fill: #E31A1C;\n}\n#european_countries_e [ area <= 34895] {\n   polygon-fill: #FC4E2A;\n}\n#european_countries_e [ area <= 12890] {\n   polygon-fill: #FD8D3C;\n}\n#european_countries_e [ area <= 10025] {\n   polygon-fill: #FEB24C;\n}\n#european_countries_e [ area <= 9150] {\n   polygon-fill: #FED976;\n}\n#european_countries_e [ area <= 5592] {\n   polygon-fill: #FFFFB2;\n}",
            "cartocss_version": "2.1.1"
          }
        },
        {
          "options": {
            "urlTemplate": "http://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png",
            "minZoom": "0",
            "maxZoom": "18",
            "attribution": "&copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors"
          },
          "type": "tiled"
        }
      ]
    };

    var vizjsonWitNoLayersProperty = {};
    var vizjsonWithNoArrayObjectIntoLayersProperty = {
      "layers": {}
    };

    beforeAll(function() {
      getLayersDataWitNoLayersProperty = mcdbJsonParser.getLayersData(vizjsonWitNoLayersProperty);
      getLayersDataWithNoArrayObjectIntoLayersProperty = mcdbJsonParser.getLayersData(vizjsonWithNoArrayObjectIntoLayersProperty);
      getLayersDataWithOneWrongProperty = mcdbJsonParser.getLayersData(vizjsonWithTwoGoodsPropertiesAndOneWrongProperty);
    });

    it('should returns an empty array if vizjson does not have layers', function() {
      expect(getLayersDataWitNoLayersProperty).toEqual({layers: []});
    });
    it('should returns an empty array if vizjson.layers is not an Array Object', function() {
      expect(getLayersDataWithNoArrayObjectIntoLayersProperty).toEqual({layers: []});
    });
    it('should returns an array with only valid layers', function() {
      expect(getLayersDataWithOneWrongProperty.layers.length).toEqual(2);
    });

  });

  describe('setSQL', function() {
    var cartoDBLayer = mcdbJsonParser._cartoDBLayer;
    var vizjsonWitNoLayersProperty = {};
    var vizjsonWithNoArrayObjectIntoLayersProperty = {
      "layers": {}
    };
    var vizjsonWithSQL = {layers: [cartoDBLayer]};

    var vizjsonWithLayersButNoCartoDBLayer = {
      "layers": [
        {
          "options": {
            "urlTemplate": "http://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png",
            "minZoom": "0",
            "maxZoom": "18",
            "attribution": "&copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors"
          },
          "type": "tiled"
        }
      ]
    };

    beforeAll(function() {
      cartoDBLayer.options.sql = '';
      mcdbJsonParser.setSQL(cartoDBLayer.options.sql, vizjsonWitNoLayersProperty);
      mcdbJsonParser.setSQL(cartoDBLayer.options.sql, vizjsonWithNoArrayObjectIntoLayersProperty);
      mcdbJsonParser.setSQL(cartoDBLayer.options.sql, vizjsonWithSQL);
      mcdbJsonParser.setSQL(cartoDBLayer.options.sql, vizjsonWithLayersButNoCartoDBLayer);
    });

    it('should returns an empty array if vizjson does not have layers', function() {
      expect(_.isEqual(vizjsonWitNoLayersProperty, {layers: [cartoDBLayer]})).toBeTruthy();
    });

    it('should returns an empty array if vizjson.layers is not an Array Object', function() {
      expect(_.isEqual(vizjsonWithNoArrayObjectIntoLayersProperty, {layers: [cartoDBLayer]})).toBeTruthy();
    });

    it('should update CartoDB layer whit new query', function() {
      expect(vizjsonWithSQL.layers[0].options.sql).toEqual(cartoDBLayer.options.sql);
    });

    it('should create a CartoDB layer it not exist', function() {
      expect(vizjsonWithLayersButNoCartoDBLayer.layers[1].type).toEqual("CartoDB");
    });
  });
});


