//  minicartodb.js 1.0.0
//  events utils tests
//  [url bitbucket]
//  (c) 2017 Pablo Rodríguez, @therealpablors
//  Carto Test Frontend developer 2017
//  minicarto may be freely distributed under the MIT license.


'use strict';

var emitter = require('../../src/utils/mcdb-events');

describe('mcdb-events module', function () {
  describe('object properties', function() {
    it('should have an eventBus property', function() {
      expect(emitter.hasOwnProperty('eventBus')).toBeTruthy();
    });
  });

  describe('sendEvent', function () {
    var functionSpy;

    beforeAll(function() {
      functionSpy = jasmine.createSpy('emitterSpy');
      emitter.eventBus.on('message', functionSpy);
      emitter.eventBus.sendEvent('message', 'data');
    });

    it('should listen for events', function() {
      expect(functionSpy).toHaveBeenCalledWith('data');
    });
  });
});
