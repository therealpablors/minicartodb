'use strict';
var proxyquire = require('proxyquireify');

module.exports = function(karma) {
  karma.set({
    frameworks: ['jasmine', 'browserify'],
    files: [
      'test/**/*.spec.js'
    ],
    preprocessors: {
      'test/**/*.spec.js': ['browserify']
    },
    reporters: ['dots'],
    browsers: ['PhantomJS'],
    logLevel: 'LOG_DEBUG',
    singleRun: true,
    autoWatch: false,
    // browserify configuration
    browserify: {
      configure: function(bundle) {
        bundle
          .plugin(proxyquire.plugin)
          .require(require.resolve('./test'), {entry: true});
      },
      debug: true,
      transform: ['brfs']
    }
  });
};
